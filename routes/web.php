<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

//Route::get('/', function () {
//    if (!Auth::user()) {
//        return view('auth.login');
//    }
//});

Route::get('/','FrontendController@index')->name('home');
Route::group(['middleware' => ['auth', 'web']], function () {
    Route::get('/dashboard', function () {
        return view('admin.dashboard');
    })->name('Home');

    Route::Resource('news', 'NewsController');
    Route::Resource('category', 'CategoryController');
    Route::Resource('albumcategory', 'AlbumCategoryController');

    Route::get('/albums', 'AlbumController@getList')->name('albums');
    Route::get('albums/createalbum', 'AlbumController@getForm')->name('create_album_form');
    Route::post('albums/createalbum', 'AlbumController@postCreate')->name('create_album');
    Route::get('/deletealbum/{id}', 'AlbumController@getDelete')->name('delete_album');
    Route::get('/albums/view-album/{id}', 'AlbumController@getAlbum')->name('show_album');
    Route::get('/albums/edit-album/{id}', 'AlbumController@editAlbum')->name('edit_album');
    Route::post('albums/edit-album', 'AlbumController@updateAlbum')->name('update_album');

    Route::get('event-category', 'EventController@categoryList');
    Route::post('insert-event-category', 'EventController@insertCategory');
    Route::delete('delete-event-category/{id}', 'EventController@deleteCategory');
    Route::get('events', 'EventController@getEventList')->name("events");
    Route::get('add-event', 'EventController@loadCreateEvent');
    Route::post('add-event', 'EventController@insertEvent');
    Route::get('/edit-event/{id}', 'EventController@editEvent')->name('edit-event');
    Route::delete('delete-event/{id}', 'EventController@deleteEvent');
});
Auth::routes([
    'register' => false]);


//Auth::routes(['register' => false]);



