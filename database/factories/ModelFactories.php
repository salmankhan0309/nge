<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */


$factory->define(\App\Models\News::class, function(Faker\Generator $faker){
    static $password;
    return[
        'title'=> $faker->sentence(10),
        'description'=>$faker->sentence(30),
        'news_date'=>$faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now'),
        'category'=>$faker->randomDigit(3,8),
        'banner'=>$faker->imageUrl($width=800, $height=400, 'cats', true, 'Faker', true) // 'http://lorempixel.com/gray/800/400/cats/Faker/' Monochrome image

    ];
});
?>