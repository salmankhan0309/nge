<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class   CreateAlbumImagesDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('album_images_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('albumId')->nullable();
            $table->string('fileName')->nullable();
            $table->tinyInteger('sortIndex')->default(0);
            $table->text("caption")->nullable();
            $table->text("credit")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('album_images_details');
    }
}
