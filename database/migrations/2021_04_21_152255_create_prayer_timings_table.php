<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrayerTimingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prayer_timings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('Day');
            $table->string('Month');
            $table->time('Fajr');
            $table->time('Sunrise');
            $table->time('Dhuhr');
            $table->time('Asr');
            $table->time('Asr2');
            $table->time('Maghrib');
            $table->time('Isha');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prayer_timings');
    }
}
