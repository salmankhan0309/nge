@extends('layouts.app')
@section('content')
    @if(session()->has('success'))
        <div class="alert alert-success" id="successMessage">
            {!! session('success') !!}
        </div>
    @endif @if(session()->has('error'))
        <div class="alert alert-danger" id="errorMessage">
            {!! session('error') !!}
        </div>
    @endif
    <a href="{!! url('add-event') !!}" class="btn btn-primary mb-2">Create New Event</a>
    <div class="divide50"></div>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Date/Time</th>
            <th>Category</th>
            <th>Venue</th>
            <th colspan="2">Action</th>
        </tr>
        </thead>
        <tbody>
        @forelse($events as $event)
            <tr>
                <td>{!! $event->id !!}</td>
                <td>{!! $event->name !!}</td>
                <td>{!! \Carbon\Carbon::parse($event->eventDate)->format("d F Y")."\n".\Carbon\Carbon::parse($event->eventTime)->format("h:ia") !!}</td>
                <td>{!! isset($event->eventCategory) ? $event->eventCategory->name : "" !!}</td>
                <td>{!! $event->venue !!}</td>
                <td class="d-flex">
                    <a href="{{URL::route('edit-event', array('id'=>$event->id))}}" class="btn btn-gray">Edit
                        Event</a>
                    <form action="{!! url('/delete-event').'/'.$event->id !!}" method="post" class="d-inline"
                          onsubmit="return confirm('Are you sure want to delete?')">
                        {{ csrf_field() }}
                        @method('DELETE')
                        <button class="btn btn-red" type="submit">Delete Event</button>
                    </form>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="6" class="text-center">No events found</td>
            </tr>
        @endforelse
        </tbody>
    </table>
@endsection

