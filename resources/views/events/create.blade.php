@extends('layouts.app')

@section('content')
    <h2 class="section-title text-center">{{ __('Create Event') }}</h2>
    <div class="divide50"></div>
    <div class="form-container">
        <div class="thin">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            <form class="form-horizontal" action="{{ url('/add-event')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label class="control-label">Name</label>
                    <input type="text" name="name" class="form-control" value="{!! old("name") !!}">
                    @if ($errors->has('name'))<label class="text-danger"
                                                     for="name">{!! $errors->first('name') !!}</label>@endif
                </div>
                <div class="form-group">
                    <label class="control-label">Description</label>
                    <textarea name="description" id="event_description" cols="30" rows="10"
                              class="form-control">{!! old("description") !!}</textarea>
                    @if ($errors->has('description'))<label class="text-danger"
                                                            for="description">{!! $errors->first('description') !!}</label>@endif
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Event date</label>
                            <input type="date" name="eventDate" class="form-control" value="{!! old("eventDate") !!}">
                            @if ($errors->has('eventDate'))<label class="text-danger"
                                                                  for="eventDate">{!! $errors->first('eventDate') !!}</label>@endif
                        </div>
                    </div>
                    <div class="col-md-4 col-md-offset-1">
                        <div class="form-group">
                            <label class="control-label">Event time</label>
                            <input type="time" name="eventTime" class="form-control" value="{!! old("eventTime") !!}">
                            @if ($errors->has('eventTime'))<label class="text-danger"
                                                                  for="eventTime">{!! $errors->first('eventTime') !!}</label>@endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Venue</label>
                            <input type="text" name="venue" class="form-control" value="{!! old("venue") !!}">
                            @if ($errors->has('venue'))<label class="text-danger"
                                                              for="venue">{!! $errors->first('venue') !!}</label>@endif
                        </div>
                    </div>
                    <div class="col-md-7 col-md-offset-1">
                        <div class="form-group">
                            <label class="control-label">Venue address</label>
                            <input type="text" name="venueAddress" class="form-control"
                                   value="{!! old("venueAddress") !!}">
                            @if ($errors->has('venueAddress'))<label class="text-danger"
                                                                     for="venueAddress">{!! $errors->first('venueAddress') !!}</label>@endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label class="control-label">Venue phone</label>
                            <input type="text" name="venuePhone" class="form-control" value="{!! old("venuePhone") !!}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label class="control-label">Organizer name</label>
                            <input type="text" name="organizerName" class="form-control"
                                   value="{!! old("organizerName") !!}">
                            @if ($errors->has('organizerName'))<label class="text-danger"
                                                                      for="organizerName">{!! $errors->first('organizerName') !!}</label>@endif
                        </div>
                    </div>
                    <div class="col-md-6 col-md-offset-1">
                        <div class="form-group">
                            <label class="control-label">Organizer phone</label>
                            <input type="text" name="organizerPhone" class="form-control"
                                   value="{!! old("organizerPhone") !!}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label class="control-label">Organizer email</label>
                            <input type="text" name="organizerEmail" class="form-control"
                                   value="{!! old("organizerEmail") !!}">
                        </div>
                    </div>
                    <div class="col-md-6 col-md-offset-1">
                        <div class="form-group">
                            <label class="control-label">Organizer website</label>
                            <input type="text" name="organizerWebsite" class="form-control"
                                   value="{!! old("organizerWebsite") !!}">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label">Select Event Category</label>
                    <select name="categoryId" class="form-control">
                        <option value="">--Select category</option>
                        @foreach($categories as $cat)
                            <option
                                value="{!! $cat->id !!}" {!! old("categoryId") == $cat->id ? 'selected' : ''  !!}>{!! $cat->name !!}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('categoryId'))<label class="text-danger"
                                                           for="categoryId">{!! $errors->first('categoryId') !!}</label>@endif
                </div>
                <div class="form-group">
                    <label class="control-label">Event Banner</label>
                    <input class="form-control" type="file" name="bannerImage">
                    @if ($errors->has('bannerImage'))<label class="text-danger"
                                                            for="bannerImage">{!! $errors->first('bannerImage') !!}</label>@endif
                </div>
                <div class="form-group text-right">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <a href="{!! url('/events') !!}" class="btn btn-gray">Cancel</a>
                </div>
            </form>

        </div>
    </div>
@endsection
