@extends('layouts.app')

@section('content')
    <a href="{{ url('news/create') }}" class="btn btn-primary mb-2">Create Post</a>
    <div class="divide50"></div>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Id</th>
            <th>Title</th>
            <th>Published At</th>
            <th>Created at</th>
            <th colspan="2">Action</th>
        </tr>
        </thead>
        <tbody>
        @forelse($news as $n)
            <tr>
                <td>{{ $n->id }}</td>
                <td>{{ $n->title }}</td>
                <td>{{ date('Y-m-d', strtotime($n->news_date)) }}</td>
                <td>{{ date('Y-m-d', strtotime($n->created_at)) }}</td>
                <td class="d-flex">
                    <a href="{!! url('/news').'/'.$n->id !!}" class="btn btn-gray">Show</a>
                    <a href="{!! url('/news').'/'.$n->id.'/edit' !!}" class="btn btn-gray">Edit</a>
                    <form action="{!! url('/news').'/'.$n->id !!}" method="post" class="d-inline" onsubmit="return confirm('Are you sure want to delete?')">
                        {{ csrf_field() }}
                        @method('DELETE')
                        <button class="btn btn-red" type="submit">Delete</button>
                    </form>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="6" class="text-center">No new(s) found</td>
            </tr>
        @endforelse
        </tbody>
    </table>
@endsection
