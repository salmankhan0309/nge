@extends('layouts.app')

@section('content')
    <h2 class="section-title text-center">{{ __('Edit News') }}</h2>
    <div class="divide50"></div>
    <div class="form-container">
        <div class="thin">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            <form class="form-horizontal" action="{{URL('/news/'.$news->id)}}" method="post"
                  enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label class="control-label">News Title</label>
                    <input type="text" name="title" class="form-control" value="{{$news->title}}">
                    @if ($errors->has('title'))<label class="text-danger"
                                                      for="title">{!! $errors->first('title') !!}</label>@endif
                </div>
                <div class="form-group">
                    <label class="control-label">News Description</label>
                    <textarea name="description" id="news_description" cols="30" rows="10"
                              class="form-control">{{$news->description}}</textarea>
                    @if ($errors->has('description'))<label class="text-danger"
                                                            for="description">{!! $errors->first('description') !!}</label>@endif
                </div>

                <div class="form-group">
                    <label class="control-label">Publish At</label>
                    <input type="date" name="news_date" class="form-control"
                           value="{{ date('Y-m-d', strtotime($news->news_date)) }}">
                </div>
                <?php
                $category = explode(",", "$news->category");
                ?>
                <div class="form-group">
                    <label class="control-label">Select News Category</label>
                    @foreach($categories as $cat)
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="category[]"
                                   value="{{ $cat->id }}" <?=(in_array($cat->id, $category)) ? "checked" : " ";?>>
                            <label class="form-check-label" for="Checkbox">{{ $cat->name }}</label>
                        </div>
                    @endforeach
                    @if ($errors->has('category'))<label class="text-danger"
                                                         for="category">{!! $errors->first('category') !!}</label>@endif
                </div>
                <div class="form-group">
                    <label class="control-label"> News Banner</label>
                    <input class="form-control" type="file" name="image">
                    @if($news->banner)
                        <div class="col-xs-6 col-md-3">
                            <div class="thumbnail">
                                <img src="{{ asset('uploads/'.$news->banner) }}"/>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="form-group text-right">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <a href="{!! url('/news') !!}" class="btn btn-gray">Cancel</a>
                </div>
            </form>
        </div>
    </div>
@endsection
