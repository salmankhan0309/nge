@extends('frontend.layout')
@section('content')

    <div id="home" class="section">
        <div class="light-wrapper">
            <div class="fullscreenbanner-container revolution">
                <div class="fullscreenbanner">
                    <ul>
                        <li data-transition="fade"> <img src="{{ asset('images/kid-1077793_1920.jpg') }}" alt="" />

                            <div class="caption large lite sfb" data-x="center" data-y="245" data-speed="900" data-start="800" data-easing="Sine.easeOut">MAKE A DONATION</div>
                            <div class="caption small lite sfb" data-x="center" data-y="319"  data-speed="900" data-start="1500" data-easing="Sine.easeOut">Help create a space dedicated to peace and beauty!</div>
                            <div class="caption small sfb" data-x="center" data-y="362" data-speed="900" data-start="2200" data-easing="Sine.easeOut">
                                <div class="smooth"><a href="#donate" class="btn btn-border-lite">Donate Now</a></div>
                            </div>
                        </li>
                        <li data-transition="fade"> <img src="{{ asset('images/quran_banner.jpg') }}" alt="" />
                            <div class="caption large lite sfb" data-x="center" data-y="245" data-speed="900" data-start="800" data-easing="Sine.easeOut">Online Classes for Children</div>
                            <div class="caption small lite sfb" data-x="center" data-y="319" data-speed="900" data-start="1500" data-easing="Sine.easeOut">Our regular classes for children have been moved completely online </div>
                            <div class="caption small sfb" data-x="center" data-y="362" data-speed="900" data-start="2200" data-easing="Sine.easeOut">
                                <div class="smooth"><a href="#contact" class="btn btn-border-lite">Contact Us</a></div>
                            </div>
                        </li>

                        <li data-transition="fade"> <img src="{{ asset('images/quran-4095475_1920.jpg') }}" alt="" />
                            <div class="caption large lite sfb" data-x="center" data-y="245" data-speed="900" data-start="800" data-easing="Sine.easeOut">We Offer Five Times Prayer</div>
                            <div class="caption small lite sfb" data-x="center" data-y="319" data-speed="900" data-start="1500" data-easing="Sine.easeOut">Please follow provincial guidelines for more information</div>
                            <div class="caption small sfb" data-x="center" data-y="362" data-speed="900" data-start="2200" data-easing="Sine.easeOut">
                                <div class="smooth"><a href="#contact" class="btn btn-border-lite">Contact Us</a></div>
                            </div>
                        </li>
                    </ul>
                    <div class="tp-bannertimer tp-bottom"></div>
                </div>
                <!-- /.fullscreenbanner -->
            </div>
            <!-- /.fullscreenbanner-container -->
        </div>
    </div>
    <div class="section ramadan-section">
        <div class="container inner">
            <div class="row">
                @foreach($news as $n)
                    @if(count($news) > 1)

                    <div class="col-sm-6">
                        <div class="col-sm-12 notice-container">
                            <div class="notice-container">
                                <h2 class="section-title text-center">{{ $n->title }}</h2>
                            </div>
                            <div class="news-description">
                                {!! $n->description !!}
                            </div>
                        </div>
                    </div>
                    @else
                        <div class="col-sm-12">
                            <div class="col-sm-12 notice-container">
                                <div class="notice-container">
                                    <h2 class="section-title text-center">{{ $n->title }}</h2>
                                </div>
                                <div class="news-description">
                                    {!! $n->description !!}
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>

    <div id="services" class="section anchor">
        <div class="light-wrapper">
            <div class="container inner">
                <h2 class="section-title text-center">Our Services</h2>
                <p class="lead main text-center">Serving Community</p>
                <div class="row text-center services-1">
                    <div class="col-sm-3">
                        <div class="col-wrapper">
                            <div class="icon-wrapper"> <img src="images/icons/islam.png" width="50"> </div>
                            <h3>Prayer/Salat</h3>
                            <p>
                                Our community centre is currently closed for prayers due to COVID-19 until further notice. We will keep you posted if any development evolves.
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="col-wrapper">
                            <div class="icon-wrapper"> <img src="images/icons/reading-quran.png" width="50"> </div>
                            <h3>Quran Lessons</h3>
                            <p>
                                We host a wide range of educational services, for children. Currently we offer only online classes for children and adults through Zoom
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="col-wrapper">
                            <div class="icon-wrapper"> <img src="images/icons/multiple-users-silhouette.png" width="50"> </div>
                            <h3>Marriage / Funeral</h3>
                            <p>
                                More details will be available soon. In case of an immediate case, contact us through Contact us page.
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="col-wrapper">
                            <div class="icon-wrapper"> <img src="images/icons/donation.png" width="50"> </div>
                            <h3>Food Distribution</h3>
                            <p>
                                Currently Friday food service is not available.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row text-center services-1 tm20">
                    <div class="col-sm-3">
                        <div class="col-wrapper">
                            <div class="icon-wrapper"> <img src="images/icons/running.png" width="50"> </div>
                            <h3>Youth and Sports Activities</h3>
                            <p>
                                Currently No sports activities available.
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="col-wrapper">
                            <div class="icon-wrapper"> <img src="images/icons/heart.png" width="50"> </div>
                            <h3>Seniors Services</h3>
                            <p>
                                In home services for seniors not available.
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="col-wrapper">
                            <div class="icon-wrapper"> <img src="images/icons/volunteer.png" width="50"> </div>
                            <h3>Join us as Volunteer</h3>
                            <p>
                                Please contact the management if you are interested in becoming a volunteer.
                            </p>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
    <!-- /.services -->


    <div class="parallax parallax1">
        <div class="container inner text-center">
            <h2 class="section-title bm50">Pillars Of Islam</h2>
            <div class="row">
                <div class="col-md-2 col-md-offset-1">
                    <div class="diamond">
                        <div class="pillars-container">
                            <img src="images/icons/faith-in-allah.png">
                            <p class="bm0">Shahadah</p>
                            <p class="sub-text">God's One-ness</p>
                        </div>

                    </div>
                </div>
                <div class="col-md-2">
                    <div class="diamond">
                        <div class="pillars-container">

                            <img src="images/icons/salat.png">
                            <p class="bm0">Salat</p>
                            <p class="sub-text">Prayer</p>

                        </div>

                    </div>
                </div>
                <div class="col-md-2">
                    <div class="diamond">
                        <div class="pillars-container">

                            <img src="images/icons/iftar.png">
                            <p class="bm0">Saum</p>
                            <p class="sub-text">Fasting</p>
                        </div>

                    </div>
                </div>
                <div class="col-md-2">
                    <div class="diamond">
                        <div class="pillars-container">

                            <img src="images/icons/give.png">
                            <p class="bm0">Zakaat</p>
                            <p class="sub-text">Giving Charity</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="diamond">
                        <div class="pillars-container">

                            <img src="images/icons/kaaba.png">
                            <p class="bm0">Hajj</p>
                            <p class="sub-text">Pilgrimage to Makkah</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container -->
    </div>
    <!-- /.parallax -->


    <div id="prayers" class="section anchor">
        <div class="white-wrapper">
            <div class="container inner">
                <h2 class="section-title text-center">Fraser Muslim Community Services</h2>
                <p class="lead main text-center">Prayer Timings</p>
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th scope="col">Prayer</th>
                        <th scope="col">Time</th>
                    </tr>
                    </thead>
                    <tbody>
                @foreach($prayer as $key=>$value)
                    @if(!in_array($key,['id','Day','Month']))
                <tr>
                    <td>{{ $key }}</td>
                    <td>{{ Date('H:i',strtotime($value)) }}
                        @if(in_array($key,['Fajr','Sunrise']))
                            AM
                        @else
                            PM
                        @endif

                    </td>
                </tr>
                    @endif
                @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- /#portfolio -->
    <div class="parallax parallax2 customers">
        <div class="container inner text-center">
            <div class="row">
                <div class="col-md-12">
                    <div class="parallax-img">
                        <img src="images/calligraphy-2789691_1280.png" width="100px">
                    </div>
                </div>
                <div class="col-md-12">
                    <h2 class="section-title">There is none worthy of worship except God (Allah) and Prophet Muhammad ﷺ is the messenger of God.</h2>
                </div>
            </div>

        </div>
    </div>
    <!-- /.container -->
    <div id="donate" class="section anchor">
        <div class="light-wrapper">
            <div class="container inner">
                <h2 class="section-title text-center">Make a donation now</h2>
                <p class="lead main text-center">Your contribution will help us to maintain and develop the wide range of services we offer. Prophet Muhammad ﷺ said: “Allah said: ‘Spend, O son of Adam, and I shall spend on you.’”
                </p>
                <div class="row">
                    <div class="col-md-10">
                        <p>We are very grateful for all donations made to Fraserview Muslim Community Services.</p>
                        <p>Please become regular monthly donor for this centre as we only depend on your donations for all of our activities.</p>
                        <p>Please donate generously as this ensures that the mosque is maintained properly and the Muslim community benefits from these facilities.</p>
                        <p>Please Note that All Donations are Tax Deductible. A Tax Receipt will be issued as soon as donation is received.</p>
                        <p>May God reward you for all your good deeds.</p>
                        <p>Jazakum Allah Khair.</p>
                    </div>
                    <div class="col-md-2">
                        <form action="https://www.paypal.com/donate" method="post" target="_blank" class="donate-button">
                            <input type="hidden" name="hosted_button_id" value="R2BNU3PQVWV4A" />
                            <input type="submit" class="btn btn-submit bm0" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Donate with PayPal button" value="Donate Now"/>
                            <!--                        <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donate_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Donate with PayPal button" />-->
                            <!--                        <img alt="" border="0" src="https://www.paypal.com/en_CA/i/scr/pixel.gif" width="1" height="1" />-->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /#about -->
    <!-- /#portfolio -->
    <div class="parallax parallax3 customers">
        <div class="container inner text-center">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="section-title">The Prophet (ﷺ) said, "If you hear of an outbreak of plague in a land, do not enter it; but if the plague breaks out in a place while you are in it, do not leave that place." <small>(Sahih al-Bukhari 5728)</small></h2>
                </div>
            </div>

        </div>
    </div>
    <div id="covid" class="section anchor">
        <div class="light-wrapper">
            <div class="container inner">
                <h2 class="section-title text-center">PLEASE FOLLOW THE COVID-19 RULES</h2>
                <p class="lead main text-center">Keep yourself informed, take precautions but at the same time do not forget that nothing happens without the will of Allah</p>
                <div class="row">
                    <div class="col-sm-8">
                        <ul class="circled">
                            <li>Perform Wudu at home as there is no facility at the premises. </li>
                            <li>Practice physical distancing all the time.</li>
                            <li>No entry without mask.</li>
                            <li>Offer only fard Salah in the facility. Pray Sunnah and Nawafils at home.</li>
                            <li>Please do not bring children under age of 14.</li>
                            <li>If you have any symptoms of cold, cough or fever, do not come to the facility.</li>
                            <li>Pray only on designated marked places.</li>
                            <li>Use hand sanitizer while entering and leaving the facility.</li>
                            <li>Follow the volunteers instructions while in the facility.</li>

                        </ul>
                        <small>Jazak Allah</small>
                    </div>
                    <div class="col-sm-4">
                        <figure><img src="{{ asset('images/8472.jpg') }}" alt="" /></figure>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="contact" class="section anchor">
        <div class="dark-wrapper">
            <div class="container inner">
                <div class="row col-container">
                    <div class="col-md-6 col">
                        <div class="thin text-center">
                            <h2 class="section-title text-center">Get In Touch</h2>
                            <p class="lead main text-center">If you have any Query, Comment, Suggestion or Complaint,
                                please use the following information to contact us.</p>
                            <!--                <p>Cras mattis consectetur purus sit amet fermentum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam porta sem malesuada magna mollis euismod. Nulla vitae elit libero, a pharetra augue. Aenean eu leo quam. Pellentesque ornare sem lacinia.</p>-->
                            <ul class="contact-info">
                                <li><i class="icon-location"></i>6436 Fraser St, Vancouver, BC V5W 3A6</li>
                                <li><i class="icon-phone"></i>+1 (604) 780-9121</li>
                                <li><i class="icon-mail"></i><a href="mailto:Fmcs786@gmail.com" target="_blank">fmcs786@gmail.com</a> </li>
                            </ul>
                            <div class="divide50"></div>
                            <!--                            <div class="form-container">-->
                            <!--                                <div class="response alert alert-success"></div>-->
                            <!--                                <form class="forms" action="config/form-handler.php" method="post">-->
                            <!--                                    <fieldset>-->
                            <!--                                        <ol class="row">-->
                            <!--                                            <li class="form-row text-input-row name-field col-sm-6">-->
                            <!--                                                <input type="text" name="name" class="text-input defaultText required" title="Name (Required)"/>-->
                            <!--                                            </li>-->
                            <!--                                            <li class="form-row text-input-row email-field col-sm-6">-->
                            <!--                                                <input type="text" name="email" class="text-input defaultText required email" title="Email (Required)"/>-->
                            <!--                                            </li>-->
                            <!--                                            <li class="form-row text-area-row col-sm-12">-->
                            <!--                                                <textarea name="message" class="text-area required" title="Your message"></textarea>-->
                            <!--                                            </li>-->
                            <!--                                            <li class="form-row hidden-row">-->
                            <!--                                                <input type="hidden" name="hidden" value="" />-->
                            <!--                                            </li>-->
                            <!--                                            <li class="nocomment">-->
                            <!--                                                <label for="nocomment">Leave This Field Empty</label>-->
                            <!--                                                <input id="nocomment" value="" name="nocomment" />-->
                            <!--                                            </li>-->
                            <!--                                            <li class="button-row">-->
                            <!--                                                <input type="submit" value="Send Message" name="submit" class="btn btn-submit bm0" />-->
                            <!--                                            </li>-->
                            <!--                                        </ol>-->
                            <!--                                        <input type="hidden" name="v_error" id="v-error" value="Required" />-->
                            <!--                                        <input type="hidden" name="v_email" id="v-email" value="Enter a valid email" />-->
                            <!--                                    </fieldset>-->
                            <!--                                </form>-->
                            <!--                            </div>-->
                            <!-- /.form-container -->
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="col-md-6 col">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d10422.607751350526!2d-123.0905783!3d49.2261277!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x7df2309f17ba710d!2sMasjid%20Fraser%20View%20Muslim%20Community!5e0!3m2!1sen!2sca!4v1617318781162!5m2!1sen!2sca" width="100%" height="400" frameborder="0" style="border:0; display:block;" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /#contact -->
@endsection