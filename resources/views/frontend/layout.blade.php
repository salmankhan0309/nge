<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Fraser Muslim Community Service') }}</title>

    <!-- Fonts -->
    <link rel="icon" href="{{ asset('images/favicon/favicon-16x16.png') }}" type="image/x-icon"/>
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('css/settings.css') }}" rel="stylesheet">
    <link href="{{ asset('css/owl.carousel.css') }}" rel="stylesheet">
    <link href="{{ asset('js/google-code-prettify/prettify.css') }}" rel="stylesheet">
    <link href="{{ asset('js/fancybox/jquery.fancybox.css') }}" rel="stylesheet">
    <link href="{{ asset('js/fancybox/helpers/jquery.fancybox-thumbs.css?v=1.0.2') }}" rel="stylesheet">
    <link href="{{ asset('css/golden.css') }}" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Josefin+Sans:100,100i,300,300i,400,400i,600,600i,700,700i'
          rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,300,500,600,700,800,900' rel='stylesheet'
          type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Dosis:200,300,400,500,600,700,800' rel='stylesheet'
          type='text/css'>
    <link href="{{ asset('type/fontello.css') }}" rel="stylesheet">
    <link href="{{ asset('type/budicons.css') }}" rel="stylesheet">
    <link href="{{ asset('js/summernote/summernote-lite.css') }}" rel="stylesheet">
    <link href="{{ asset('css/jquery.dataTables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

</head>
<body>
<div class="body-wrapper">
    <div class="navbar default">
        <div class="navbar-header">
            <div class="container">

                <div class="basic-wrapper"> <a class="btn responsive-menu pull-right" data-toggle="collapse" data-target=".navbar-collapse"><i class='icon-menu-1'></i></a> <a class="navbar-brand" href="{{ url('/') }}"><img src="{{ asset('images/746321.png') }}" alt="" data-src="{{ asset('images/746321.png') }}" data-ret="{{ asset('images/746321.png') }}" class="retina" /></a> </div>

                <nav class="collapse navbar-collapse pull-right">
                    <ul class="nav navbar-nav">
                        <li><a href="#home">Home</a></li>
                        <li><a href="#services">Services</a></li>
                        <li><a href="#prayers">Prayer Timing</a></li>
                        <li><a href="#donate">Donate</a></li>
                        <li><a href="#covid">Covid19</a></li>
                        <li><a href="#contact">Contact</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>

    @yield('content')

    <footer class="footer">
        <div class="container inner">
            <p class="pull-left">© <?=date('Y')?> Fraserview Muslim Community Services . All rights reserved.</p>
            <ul class="social pull-right">
                <li><a href="https://www.facebook.com/thefmcsCanada/"><i class="icon-s-facebook"></i></a></li>
            </ul>
        </div>
        <!-- .container -->
    </footer>
</div>

<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/twitter-bootstrap-hover-dropdown.min.js') }}"></script>
<script src="{{ asset('js/jquery.themepunch.plugins.min.js') }}"></script>
<script src="{{ asset('js/jquery.themepunch.revolution.min.js') }}"></script>
<script src="{{ asset('js/jquery.easytabs.min.js') }}"></script>
<script src="{{ asset('js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('js/jquery.isotope.min.js') }}"></script>
<script src="{{ asset('js/jquery.fitvids.js') }}"></script>
<script src="{{ asset('js/jquery.fancybox.pack.js') }}"></script>
<script src="{{ asset('js/fancybox/helpers/jquery.fancybox-thumbs.js?v=1.0.2') }}"></script>
<script src="{{ asset('js/fancybox/helpers/jquery.fancybox-media.js?v=1.0.0') }}"></script>
<script src="{{ asset('js/jquery.slickforms.js') }}"></script>
<script src="{{ asset('js/instafeed.min.js') }}"></script>
<script src="{{ asset('js/retina.js') }}"></script>
<script src="{{ asset('js/google-code-prettify/prettify.js') }}"></script>
<script src="{{ asset('js/scripts.js') }}"></script>
<script src="{{ asset('js/admin.js') }}"></script>
<script src="{{ asset('js/summernote/summernote-lite.js') }}"></script>
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script>
    // var myDiv = $('.news-description');
    // console.log(myDiv.text().length);
    // if(myDiv.text().length > 300){
    //     myDiv.html(myDiv.html().substring(0,900)+ '<a href="#">Read more</a>')
    // }
</script>
@yield('script')
</body>
</html>
