<?php

namespace App\Http\Controllers;

use App\Models\News;
use App\Models\PrayerTiming;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
    public function index(){
        $news = News::all();
        $prayer = PrayerTiming::where([['Day',date('j')],['Month',date('F')]])->first()->toArray();
        return view('frontend.index',compact('prayer','news'));
    }
}
