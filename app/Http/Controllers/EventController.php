<?php

namespace App\Http\Controllers;

use App\Models\EventCategory;
use App\Models\EventDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Validator;
use Illuminate\Support\Facades\File;

class EventController extends Controller
{
    /**
     * Display a listing of the category.
     *
     * @return \Illuminate\Http\Response
     */
    public function categoryList(Request $request)
    {
        if ($request->ajax()) {
            $data = EventCategory::latest()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->editColumn('created_at', function ($row) {
                    return date('d-m-Y', strtotime($row->created_at));
                })
                ->addColumn('action', function ($row) {
                    $btn = ' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $row->id . '" data-original-title="Delete" class="btn btn-red btn-sm deleteProduct">delete</a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }


        return view('events.category');
    }

    /**
     * Store new event category
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function insertCategory(Request $request)
    {
        $request->validate([
            'category' => 'required',
        ]);

        $category = new EventCategory();
        $category->name = $request->category;
        $category->save();
        return response()->json(['success' => 'Category Added.']);

    }

    /**
     * Remove event category
     *
     * @param \App\Category s $category
     * @return \Illuminate\Http\Response
     */
    public function deleteCategory($id)
    {
        $category = EventCategory::find($id);
        if ($category) {
            $category->delete();
            return response()->json(['success' => 'Category deleted successfully.']);
        }
    }

    /**
     * Event list
     */
    public function getEventList()
    {
        $events = EventDetail::all();
        return view('events.index', compact('events'));
    }

    /**
     * Load create event page
     */
    public function loadCreateEvent()
    {
        $categories = EventCategory::all();
        return view('events.create', compact('categories'));
    }

    /**
     * Insert event
     */
    public function insertEvent(Request $request)
    {
        $element_array = array(
            'name' => 'required',
            'description' => 'required',
            'eventDate' => 'required',
            'eventTime' => 'required',
            'venue' => 'required',
            'venueAddress' => 'required',
            'organizerName' => 'required',
            'categoryId' => 'required'
        );
        $event_id = $request->input("event_id");
        if ($event_id == null) {
            $data = new EventDetail();
            $element_array['bannerImage'] = 'required';
        } else {
            $data = EventDetail::find($event_id);
        }
        $validator = Validator::make($request->all(), $element_array, [
            'name.required' => 'Please enter name',
            'description.required' => 'Please enter description',
            'eventDate.required' => 'Please enter event date',
            'eventTime.required' => 'Please enter event time',
            'venue.required' => 'Please enter venue',
            'venueAddress.required' => 'Please enter venue address',
            'organizerName.required' => 'Please enter organizer name',
            'categoryId.required' => 'Please enter category',
            'price.required' => 'Please select price',
            'bannerImage.required' => 'Please select banner image'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
        }
        $data->name = $request->input("name");
        $data->description = $request->input("description");
        $data->eventDate = Carbon::parse($request->input("eventDate"))->format("Y-m-d");
        $data->eventTime = $request->input("eventTime");
        $data->venue = $request->input("venue");
        $data->venueAddress = $request->input("venueAddress");
        $data->venuePhone = $request->input("venuePhone");
        $data->organizerName = $request->input("organizerName");
        $data->organizerPhone = $request->input("organizerPhone");
        $data->organizerEmail = $request->input("organizerEmail");
        $data->organizerWebsite = $request->input("organizerWebsite");
        $data->categoryId = $request->input("categoryId");
        $data->save();

        if ($request->file("bannerImage")) {
            $image = $request->file("bannerImage");
            $image_name = $data->id . "." . $image->clientExtension();
            $destination_path = public_path("uploads/event/");

            if (!File::exists($destination_path)) {
                File::makeDirectory($destination_path);
            }
            $image->move($destination_path, $image_name);
            $data->bannerImage = $image_name;
            $data->save();
        }
        $msg = ("Event " . ($event_id == null ? "created" : "updated") . " successfully");
        return redirect()->route("events")->with("success", $msg);
    }

    /**
     * Edit event
     */
    public function editEvent($id)
    {
        $data = EventDetail::find($id);
        if ($data) {
            $categories = EventCategory::all();
            return view('events.edit', compact('categories', 'data'));
        }
    }

    /**
     * Delete event
     */
    public function deleteEvent($id)
    {
        if ($id != null) {
            $event_data = EventDetail::find($id);
            if ($event_data) {
                if ($event_data->bannerImage != null && file_exists(public_path("uploads/event/" . $event_data->bannerImage))) {
                    unlink(public_path("uploads/event/" . $event_data->bannerImage));
                }
                $event_data->delete();
                return redirect()->route("events")->with("success", "Event deleted successfully");
            }
        }
    }
}
